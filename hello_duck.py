from flask import Flask
import mysql.connector

app = Flask(__name__)

@app.route('/')
def hello_world():
    db = mysql.connector.connect(
        host="mariadb-service",
        user="root",
        password="my_password",
        database="testdb",
        port="33066"
    )

    # cursor = db.cursor()
    # cursor.execute("SELECT VERSION()")

    # data = cursor.fetchone()

    # return "Database version: {}".format(data[0])

    cursor = db.cursor()
    cursor.execute("SELECT * FROM ducks")

    data = cursor.fetchall()
    
    return f'hello the ducks: {data}'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
